FROM centos:centos7

LABEL maintainer.name=Artem \
    maintainer.surname=Makhno \
    maintainer.email=<netoen@yandex.ru>

ARG YUMPACKAGES='python3 mc nano git sshpass' \
    YUMPARAMS='-y ' \
    PIPPACKAGES='ansible >= 4.5, <= 5.2.0\nansible-lint' \
    PIPREQFILENAME='requirements.pip'
    # reqpackages='epel-release'

# first two lines fix centos 8 repository issue ... temporarily
#  need to move to a centos-stream docker image once it exists
# RUN sed -i -e "s|mirrorlist=|#mirrorlist=|g" /etc/yum.repos.d/CentOS-* && \
#     sed -i -e "s|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g" /etc/yum.repos.d/CentOS-* && \
RUN yum update -y && \
    yum install epel-release -y && \
    yum update -y
RUN yum install $YUMPARAMS $YUMPACKAGES && \
    pip3 install --upgrade pip wheel && \
    echo -e $PIPPACKAGES > $PIPREQFILENAME && pip3 install -r $PIPREQFILENAME && \
    yum clean all -y && rm -rf /var/cache/yum && rm -rf .cache

# ENV ENV_VAR "value"

# EXPOSE 80

# SHELL ["/bin/bash", "-c"]

# ADD src/file.cpp /usr/include/mylib/file.cpp

# ONBUILD RUN /usr/sbin/nologin

# COPY src/file.cpp /usr/include/mylib/file.cpp

# RUN /usr/sbin/nologin

# WORKDIR /path/to/workdir

# USER nobody

# ENTRYPOINT top -b

# CMD /usr/bin/default_cmd

# STOPSIGNAL SIGTERM

# HEALTHCHECK --interval=15 --timeout=60 --retries=5 CMD [ "/usr/bin/my_health_check_script", "arg_1" ]