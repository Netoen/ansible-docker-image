FROM ubuntu:22.04

LABEL maintainer.name=Artem \
    maintainer.surname=Makhno \
    maintainer.email=<netoen@yandex.ru>

SHELL ["/bin/bash", "-c"]

ARG APTPACKAGES='python3 python3-pip mc nano git sshpass' \
    APTPARAMS='-y ' \
    PIPPACKAGES='ansible\nansible-lint\ntldr' \
    PIPREQFILENAME='requirements.pip'

RUN apt update && apt upgrade -y
RUN apt install $APTPARAMS $APTPACKAGES && apt-get clean
RUN python3 -m pip install --upgrade pip wheel
RUN echo -e $PIPPACKAGES > $PIPREQFILENAME && python3 -m pip install -r $PIPREQFILENAME
ENV LC_ALL C.utf8

# ENV ENV_VAR "value"

# EXPOSE 80

# ADD src/file.cpp /usr/include/mylib/file.cpp

# ONBUILD RUN /usr/sbin/nologin

# COPY src/file.cpp /usr/include/mylib/file.cpp

# RUN /usr/sbin/nologin

# WORKDIR /path/to/workdir

# USER nobody

# ENTRYPOINT top -b

# CMD /usr/bin/default_cmd

# STOPSIGNAL SIGTERM

# HEALTHCHECK --interval=15 --timeout=60 --retries=5 CMD [ "/usr/bin/my_health_check_script", "arg_1" ]